/* GLib memory tracking preload library
 * Copyright (C) 2007,2008 Nokia Corporation and its subsidary(-ies)
 *               contact: <stefan.kost@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * gcc --shared -g `pkg-config --cflags --libs glib-2.0` mp.c -olibmempattern.so
 * sudo cp libmempattern.so /usr/lib/
 */
 
/* TODO:
 * better backtraces:
 * http://blogs.linux.ie/caolan/2007/04/16/backtraces-and-prelink/
 * http://people.redhat.com/caolanm/backtraces/backtrace.c
 *
 * need to have logs per pid
 */

#include <malloc.h>
#include <signal.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#include <glib.h>

/* log file names */
static gchar *mp_log_base;
static gchar *mp_time_log_name=NULL;
static gchar *mp_frag_log_name=NULL;
static gchar *mp_use_log_name=NULL;
static gchar *mp_bt_log_name=NULL;

#define MP_BACKTRACE_DEPTH 8

static gsize mp_trace_size = 0;
static gboolean mp_trace_interactive = TRUE;
static gdouble mp_log_start = 0.0;

/* backtraces */


/* timestamps */

static gdouble its = 0.0;

static gdouble
mp_get_timestamp (void)
{
  struct timespec ts;

  clock_gettime(CLOCK_MONOTONIC, &ts);
  return (((gdouble) ts.tv_sec+ (gdouble) ts.tv_nsec * 1.0e-9) - its);
}


/* list handling */

#define MP_PAD 32
#define MP_COOKIE 0xdeadbeaf
typedef struct {
  gulong cookie;
  gpointer next;
  gpointer prev;
  gsize size;
  gdouble ts;
} MpNode;

#define REAL_TO_PADDED(real) (real?(real-MP_PAD):0)
#define PADDED_TO_REAL(pad) (pad+MP_PAD)

// need list,sizes per thread?
static MpNode *list = NULL;
static guint64 asize = 0, usize = 0;

static void
mp_list_insert_entry (MpNode *node)
{
  // prepend
  if (list) {
    node->next = list;
    list->prev = node;
  }
  list = node;

  // update sumary
  asize += node->size;
  usize += malloc_usable_size (node);
}

static void
mp_list_remove_entry (MpNode *node)
{
  if (node->next) {
    MpNode *link=(MpNode *)(node->next);
    
    //if (link->cookie == MP_COOKIE)
      link->prev = node->prev;
    //else
    //  fprintf(stderr,"broken next link\n");
  }

  if (node->prev) {
    MpNode *link=(MpNode *)(node->prev);
    
    //if (link->cookie == MP_COOKIE)
      link->next = node->next;
    //else
    //  fprintf(stderr,"broken prev link\n");
  }
  else
    list = node->next;
  

  // update sumary
  asize -= node->size;
  usize -= malloc_usable_size (node);
}


/* fragment logs */

static void
mp_log_allocs (void)
{
  FILE *log;

  if ((log=fopen(mp_frag_log_name,"wt"))) {
    MpNode *cur;
    guint i;

    // walk the list
    for (cur = list; cur; cur = cur->next) {
      // FIXME: use malloc_usable_size(cur->entry) instead of cur->size
      fprintf(log,
        //"%6"G_GSIZE_FORMAT", %12"G_GUINT64_FORMAT", %12"G_GUINT64_FORMAT" %p\n",
        "%6"G_GSIZE_FORMAT" %lf %lf %p\n",
        cur->size, cur->ts, mp_get_timestamp(), PADDED_TO_REAL(cur));
    }
    fclose (log);
  }
  else {
    fprintf (stderr, "Can't open %s : %s\n", mp_frag_log_name, strerror (errno));
  }
}

/* usage logs */

static gint mp_usage_scale = 1;

static void
mp_log_usage(gdouble ts)
{
  FILE *use_log;

  if (ts >= mp_log_start) {
    // FIXME: maybe number those, or have a timestamp in the name
    if ((use_log = fopen (mp_use_log_name, "at"))) {
      gchar ts_str[G_ASCII_DTOSTR_BUF_SIZE];
  
      g_ascii_dtostr(ts_str,G_ASCII_DTOSTR_BUF_SIZE,ts);
      fprintf(use_log,
        "%s %"G_GUINT64_FORMAT" %"G_GUINT64_FORMAT"\n",
        ts_str, asize/mp_usage_scale, usize/mp_usage_scale);
      fclose(use_log);
    }
    else {
      fprintf (stderr, "Can't open %s : %s\n", mp_use_log_name, strerror (errno));
    }
  }
}

/* signals */

static void
ms_frag_log_handler (int signum, siginfo_t * si, void *misc)
{
  switch (si->si_signo) {
    case SIGHUP:
      mp_log_allocs();
      break;
    default:
      printf ("signo:  %d\n", si->si_signo);
      printf ("errno:  %d\n", si->si_errno);
      printf ("code:   %d\n", si->si_code);
      break;
  }
}

static void
signal_setup (void)
{
  struct sigaction action;

  memset (&action, 0, sizeof (action));
  action.sa_sigaction = ms_frag_log_handler;
  action.sa_flags = SA_SIGINFO;
  sigaction (SIGHUP, &action, NULL);
}

/* tracking */

static void
mp_start_entry(gpointer entry, gsize size)
{
  gdouble ts;
  MpNode *node;
  
  if (!size)
    return;
  
  ts = mp_get_timestamp();
  node = (MpNode *)entry;
  node->cookie = MP_COOKIE;
  node->next = node->prev = NULL;
  node->size = size;
  node->ts = ts;
  mp_list_insert_entry (node);
  mp_log_usage(ts);

  if( size == mp_trace_size) {
    if (mp_trace_interactive) {
      gpointer trace[MP_BACKTRACE_DEPTH];
      gint i, count = backtrace (trace, MP_BACKTRACE_DEPTH);
      gchar **strings = backtrace_symbols (trace, count);

      printf ("Allocating %d byte(s) at %lf sec\n", size, ts);
      for (i = 0; i < count; i++)
        printf ("  %s\n", strings[i]);
      free (strings);
      g_on_error_query (NULL);
      // I don't get meaningful backtraces from this :/
      //G_BREAKPOINT();
    }
    else {
      FILE *log;

      if ((log = fopen (mp_bt_log_name,"at"))) {
        gpointer trace[MP_BACKTRACE_DEPTH];
        gint i, count = backtrace (trace, MP_BACKTRACE_DEPTH);

        fprintf (log,"%p", trace[0]);
        for (i = 1; i < count; i++)
          fprintf (log,", %p", trace[i]);
        fprintf (log,"\n", trace[i]);
        fclose(log);
      }
      else {
        fprintf (stderr, "Can't open %s : %s\n", mp_bt_log_name, strerror (errno));
      }
    }
  }
}

static void
mp_end_entry(gpointer entry)
{
  gdouble ts;
  MpNode *node;

  if (!entry)
    return;

  node = (MpNode *)entry;
  if (node->cookie == MP_COOKIE) {
    mp_list_remove_entry (node);
    node->cookie = 0;
    
    ts = mp_get_timestamp();
    mp_log_usage(ts);
    /* only log those >= requested start time */
    if (node->ts >= mp_log_start) {
      FILE *time_log;
  
      if ((time_log = fopen (mp_time_log_name, "at"))) {
        gchar tss[G_ASCII_DTOSTR_BUF_SIZE],tse[G_ASCII_DTOSTR_BUF_SIZE];
  
        g_ascii_dtostr(tss,G_ASCII_DTOSTR_BUF_SIZE,node->ts);
        g_ascii_dtostr(tse,G_ASCII_DTOSTR_BUF_SIZE,ts);
        fprintf(time_log,
          //"%6"G_GSIZE_FORMAT", %12"G_GUINT64_FORMAT", %12"G_GUINT64_FORMAT" %p\n",
          "%6"G_GSIZE_FORMAT" %s %s %p\n",
          node->size, tss, tse, PADDED_TO_REAL(entry));
        fclose(time_log);
      }
      else {
        fprintf (stderr, "Can't open %s : %s\n", mp_time_log_name, strerror (errno));
      }
    }
  }
  else {
    fprintf(stderr,"record for %p lost\n",entry);
  }
}


/* logging memtable overrides */

static gpointer
mp_malloc (gsize n_bytes)
{
  if (n_bytes) {
    gpointer mem = malloc (n_bytes + MP_PAD);
  
    //fprintf(stderr,"malloc\n");
    
    mp_start_entry (mem, n_bytes);
    return (PADDED_TO_REAL (mem));
  }
  else {
    return (malloc(0));
  }
}
 
static gpointer
mp_realloc(gpointer real, gsize n_bytes)
{
  gpointer mem = REAL_TO_PADDED (real);
  gpointer mem_new;
  MpNode *node;

  node = (MpNode *)mem;
  if (node && node->cookie == MP_COOKIE) {
    mp_end_entry (mem);
  }
  else {
    mem=real;
  }

  if (n_bytes) {
    mem_new = realloc (mem, (n_bytes + MP_PAD));
    mp_start_entry (mem_new, n_bytes);
    return (PADDED_TO_REAL (mem_new));
  }
  else {
    return (realloc (mem, 0));
  }
}

static void
mp_free (gpointer real)
{
  gpointer mem = REAL_TO_PADDED(real);

  mp_end_entry (mem);
  free (mem);
}


/* init/free */

static void
#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ > 4)
__attribute__ ((constructor))
#endif /* !__GNUC__ */
mp_init (void)
{
  GMemVTable mp_mem_table = {
    mp_malloc,
    mp_realloc,
    mp_free,
    NULL,
    NULL,
    NULL,
  };
  gchar *envvar;
  int sl;

  if( (envvar = getenv ("MP_TRACE_SIZE"))) {
    mp_trace_size = atoi (envvar);
  }
  if( (envvar = getenv ("MP_TRACE_INTERACTIVE"))) {
    mp_trace_interactive = atoi (envvar);
  }
  if( (envvar = getenv ("MP_USAGE_SCALE"))) {
    mp_usage_scale = atoi (envvar);
  }
  if( (envvar = getenv ("MP_LOG_START"))) {
    mp_log_start = atof (envvar);
  }
  if(!(mp_log_base = getenv ("MP_LOG_BASE"))) {
    const gchar *tmpdir=g_get_tmp_dir();
    mp_log_base = malloc(strlen(tmpdir)+strlen("/mp_")+1);
    strcpy (mp_log_base, tmpdir);
    strcat (mp_log_base, "/mp_");
  }
  sl=strlen(mp_log_base);

  mp_time_log_name = malloc (sl + strlen ("time.log") + 1);
  strcpy (mp_time_log_name, mp_log_base);
  strcat (mp_time_log_name, "time.log");

  mp_frag_log_name = malloc (sl + strlen ("frag.log") + 1);
  strcpy (mp_frag_log_name, mp_log_base);
  strcat (mp_frag_log_name, "frag.log");

  mp_use_log_name = malloc (sl + strlen ("use.log") + 1);
  strcpy (mp_use_log_name, mp_log_base);
  strcat (mp_use_log_name, "use.log");
  
  mp_bt_log_name = malloc (sl + strlen ("bt.log") + 1);
  strcpy (mp_bt_log_name, mp_log_base);
  strcat (mp_bt_log_name, "bt.log");

  unlink (mp_time_log_name);
  unlink (mp_frag_log_name);
  unlink (mp_use_log_name);
  unlink (mp_bt_log_name);
  
  /*
  if (!(time_log = fopen (MP_TIME_LOG, "wt"))) {
    fprintf(stderr,"cannot open "MP_TIME_LOG" for writing\n");
    return;
  }
  if (!(use_log = fopen (MP_USE_LOG, "wt"))) {
    fprintf(stderr,"cannot open "MP_USE_LOG" for writing\n");
    return;
  }
  */

  signal_setup();
  g_mem_set_vtable (&mp_mem_table);
  its = mp_get_timestamp();
}

static void
#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ > 4)
__attribute__ ((destructor))
#endif /* !__GNUC__ */
mp_done (void)
{
  /*
  fclose(time_log);
  fclose(use_log);
  */
}
