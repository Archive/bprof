/* GLib memory tracking preload library
 * Copyright (C) 2007,2008 Nokia Corporation and its subsidary(-ies)
 *               contact: <stefan.kost@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * gcc -g `pkg-config --cflags --libs glib-2.0` test.c -otest
 * G_SLICE=always-malloc G_DEBUG=gc-friendly LD_PRELOAD=libmempattern.so ./test
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <glib.h>

int
main (int argc,char *argv) {
  gpointer test;

  test = g_malloc(5);
  usleep(10);
  g_free(test);
  usleep(2);

  test = g_malloc(15);
  usleep(3);
  g_free(test);

  test = g_malloc(5);
  usleep(10);
  g_free(test);

  return 0;
}

